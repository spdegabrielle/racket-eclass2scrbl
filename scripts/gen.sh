#!/bin/sh


# This file is part of racket-eclass2scrbl - Eclass to Scribble converter.
# Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v2 License
# SPDX-License-Identifier: GPL-2.0-or-later

# racket-eclass2scrbl is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# racket-eclass2scrbl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-eclass2scrbl.  If not, see <https://www.gnu.org/licenses/>.


# This is a script generating Eclass documentation for the Gentoo repository.


# No "set -e".
trap 'exit 128' INT
export PATH


root_source="$(realpath "$(dirname "${0}")/../")"
root_cache="${root_source}"/.cache
root_scripts="${root_source}"/scripts

gentoo_repo_path="$(portageq get_repo_path / gentoo)"


mkdir -p "${root_cache}"
cd "${root_cache}" || exit 1

for eclass_file in "${gentoo_repo_path}"/eclass/*.eclass ; do
    eclass_name="$(basename "$(basename "${eclass_file}")" .eclass)"
    eclass_scrbl="${root_cache}"/"${eclass_name}".scrbl

    echo "[ .. ] Working on ${eclass_name} (${eclass_file}) ..."

    "${root_scripts}"/run.sh "${eclass_file}"

    ! scribble --dest "${root_cache}" --markdown --quiet \
      "${eclass_scrbl}" >/dev/null 2>&1 &&
        echo "[ !! ] Scribble failed to transform ${eclass_name}."
done
