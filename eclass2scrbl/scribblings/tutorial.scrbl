;; This file is part of racket-eclass2scrbl - Eclass to Scribble converter.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-eclass2scrbl is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-eclass2scrbl is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-eclass2scrbl.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (only-in scribble/bnf nonterm)
          (for-label racket))


@title[#:tag "eclass2scrbl-tutorial"]{Tutorial}

@itemlist[
 @item{Find the location of an overlay repository
  that You want to generate an eclass documentation from.

  The tool @exec{portageq} can help in finding paths
  of installed repositories.
  Execute it with arguments:
  @nonterm{get_repo_path}, @nonterm{/}
  and @nonterm{name-of-the-installed-repository}

  In this example we will use the main @exec{::gentoo} repository:

  @commandline{
   portageq get_repo_path / gentoo}}

 @item{Check if your eclass is in the selected repository.

  @commandline{
   ls -1 "$(portageq get_repo_path / gentoo)"/eclass/*.eclass}}

 @item{Give the path to an eclass to @exec{eclass2scrbl}.

  In this example we will use the "toolchain-funcs" eclass:

  @commandline{
   eclass2scrbl
   "$(portageq get_repo_path / gentoo)"/eclass/toolchain-funcs.eclass}}

 @item{Eclass2Scrbl should have generated a "eclass-name.scrbl"
  (so, file "toolchain-funcs.scrbl" from the last example).
  Using that scribble file You can now generate any of documentation formats
  supported by Scribble.

  In this example we will generate a Markdown document:

  @commandline{
   scribble --markdown --quiet toolchain-funcs.scrbl}}
 ]
