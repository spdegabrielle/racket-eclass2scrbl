;; This file is part of racket-eclass2scrbl - Eclass to Scribble converter.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-eclass2scrbl is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-eclass2scrbl is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-eclass2scrbl.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual


@title[#:tag "eclass2scrbl-references"]{References}

@itemlist[
 @item{Eclasses from the @exec{::gentoo} repository can be found on
  @link["https://gentoo-racket.gitlab.io/racket-eclass2scrbl/gentoo.html"
        "gentoo-racket.gitlab.io/racket-eclass2scrbl/gentoo.html"]
  (not guaranteed this link will work in the future).}
 @item{@link["https://devmanual.gentoo.org/eclass-writing"
             "Eclass writing documentation"]}
 ]
